package com.example.asus_pc.weather;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.asus_pc.weather.Api.ServicesTutorial;
import com.example.asus_pc.weather.adapters.CityListAdapter;
import com.example.asus_pc.weather.models.Weather;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {
    private Activity activity = this;
    private ServicesTutorial servicesTutorial;
    private CityListAdapter adapter;
    private List<Weather> cities = new ArrayList<>();
    private LinearLayout linearLayout;
    private TextView cityName;
    private TextView date;
    private TextView clock;
    private TextView weekDay;
    private TextView temperature;
    private ImageView imageWeather;
    private TextView fahrenheit;
    private TextView wind;
    private TextView humidity;
    private TextView pressure;
    private ListView listCity;
    private String citiesNames[] = {"Erevan", "London", "Paris", "Tbilisi", "Moscow", "Gyumri"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        linearLayout = findViewById(R.id.weatherInfo);
        linearLayout.setVisibility(View.GONE);
        cityName = findViewById(R.id.cityName);
        date = findViewById(R.id.date);
        clock = findViewById(R.id.clock);
        weekDay = findViewById(R.id.weekDay);
        temperature = findViewById(R.id.temperature);
        imageWeather = findViewById(R.id.imageWeather);
        fahrenheit = findViewById(R.id.fahrenheit);
        wind = findViewById(R.id.wind);
        humidity = findViewById(R.id.humidity);
        pressure = findViewById(R.id.pressure);

        listCity = findViewById(R.id.listCity);

        for (int i = 0; i < citiesNames.length; i++) {
            getData(citiesNames[i]);
        }

        listCity.setOnItemClickListener((parent, view, position, id) -> {
            linearLayout.setVisibility(View.VISIBLE);
            Weather model = cities.get(position);

            String arrLocaltime[] = model.getLocation().getLocaltime().split(" ");
            String strDate[] = arrLocaltime[0].split("-");
            int year = Integer.parseInt(strDate[0]);
            int month = Integer.parseInt(strDate[1]);
            int day = Integer.parseInt(strDate[2]);

            cityName.setText(model.getLocation().getName());
            date.setText(day + "/" + month + "/" + year);
            clock.setText(arrLocaltime[1]);
            weekDay.setText("Tuesday");
            temperature.setText(model.getCurrent().getTempC() + "°C");
            imageWeather.setImageResource(R.drawable.sunny_to_partly_cloudy_icon);
            fahrenheit.setText(model.getCurrent().getTempF() + "°F");
            wind.setText(model.getCurrent().getWindKph() + " kph");
            humidity.setText(model.getCurrent().getHumidity() + "%");
            pressure.setText(model.getCurrent().getPressureMb() + " mb");
        });
    }

    private void getData(String city) {
        servicesTutorial = ServicesTutorial.retrofit.create(ServicesTutorial.class);
        Call<Weather> call = servicesTutorial.getCity(city);
        call.enqueue(new Callback<Weather>() {
            @Override
            public void onResponse(Call<Weather> call, Response<Weather> response) {
                cities.add(response.body());
                adapter = new CityListAdapter(activity, cities);
                listCity.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<Weather> call, Throwable t) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.not_found_internet_connection), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
