package com.example.asus_pc.weather.Api;

import com.example.asus_pc.weather.models.Weather;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ServicesTutorial {
    @GET("current.json?key=0ae6c7fcb5b648b38be211611180502")
    Call<Weather> getCity(@Query("q") String city);

    public static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://api.apixu.com/v1/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
}
