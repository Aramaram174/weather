package com.example.asus_pc.weather.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.asus_pc.weather.R;
import com.example.asus_pc.weather.models.Weather;

import java.util.List;

public class CityListAdapter extends BaseAdapter {
    private Context ctx;
    private LayoutInflater lInflater;
    private List<Weather> objects;
    private Weather weather;

    public CityListAdapter(Context context, List<Weather> products) {
        ctx = context;
        objects = products;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.city_list_adapter_view, parent, false);
        }
        weather = getProduct(position);

        String time;
        String city;
        String temperature;

        if (weather.getLocation().getLocaltime() == null || weather.getLocation().getLocaltime() == ""){
            time = "--:--";
        }else {
            String arrLocaltime[] = weather.getLocation().getLocaltime().split(" ");
            time = arrLocaltime[1];
        }

        if (weather.getLocation().getName() == null || weather.getLocation().getName() == "" ) {
            city = "-----";
        }else {
            city = weather.getLocation().getName();
        }

        if (weather.getCurrent().getTempC() == null || weather.getCurrent().getTempC().equals("")){
            temperature = "°C";
        }else {
            temperature = weather.getCurrent().getTempC() + "°C";
        }

        ((TextView) view.findViewById(R.id.textViewClock)).setText(time);
        ((TextView) view.findViewById(R.id.textViewCity)).setText(city);
        ((TextView) view.findViewById(R.id.textViewTemperature)).setText(temperature);
        return view;
    }

    private Weather getProduct(int position) {
        return ((Weather) getItem(position));
    }
}

